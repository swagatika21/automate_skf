from behave import *
from features.pages.next_button_page import VerifyNextButton


@step("select {bearing_type} to bearing type dropdown")
def step_impl(context, bearing_type):
    VerifyNextButton(context.driver).select_dropdown(bearing_type)


@step("Type {number} in Search designation input box")
def step_impl(context, number):
    VerifyNextButton(context.driver).search(number)


@step("Wait for table to load")
def step_impl(context):
    VerifyNextButton(context.driver).wait_for_table()


@step("Select the row showing {designation_value} under Designation header")
def step_impl(context, designation_value):
    VerifyNextButton(context.driver).select_row(designation_value)


@step("Verify Next button color has turned to Dark grey")
def step_impl(context):
    VerifyNextButton(context.driver).next_button_colour_verify()
