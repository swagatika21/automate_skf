from behave import *
from features.pages.verify_page import VerifyDropdown


@when("I navigate to application url")
def step_impl(context):
    VerifyDropdown(context.driver).open_application()


@step("Click on Accept & continue if displayed")
def step_impl(context):
    VerifyDropdown(context.driver).click_on_accept_continue()


@step("Click on {image_name} image")
def step_impl(context, image_name):
    VerifyDropdown(context.driver).click_on_image(image_name)


@step("Click on Select bearing type dropdown")
def step_impl(context):
    VerifyDropdown(context.driver).select_bearing_type()


@then("Verify the following options are present")
def step_impl(context):
    VerifyDropdown(context.driver).verify_options(context.table)


@step("Close dropdown without selecting any option")
def step_impl(context):
    VerifyDropdown(context.driver).close_dropdown()
