Feature: SKF application Test Scenario 1

  @First
  Scenario: Launch SKF application and verify the drop down options test

    When I navigate to application url
    And Click on Accept & continue if displayed
    And Click on Single bearing image
    And Click on Select bearing type dropdown
    Then Verify the following options are present

      | Insert bearing (Y-bearing)        |
      | Angular contact ball bearing      |
      | Self-aligning ball bearing        |
      | Cylindrical roller thrust bearing |
      | Needle roller bearing             |
      | Tapered roller bearing            |
      | Spherical roller bearing          |
      | CARB toroidal roller bearing      |
      | Thrust ball bearing               |
      | Cylindrical roller thrust bearing |
      | Needle roller thrust bearing      |
      | Spherical roller thrust bearing   |
      | track roller                      |
      | Deep groove ball bearing          |

    And Close dropdown without selecting any option



