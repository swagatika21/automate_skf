from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from unittest import TestCase
testcase = TestCase()


class VerifyNextButton:

    def __init__(self, driver):
        self.driver = driver

    def select_dropdown(self, bearing_type):
        self.driver.find_element_by_css_selector("[aria-label='Select bearing type']").click()
        bearing = self.driver.find_element_by_css_selector(".mat-select-panel>mat-option>span.mat-option-text>span")
        if bearing.text == bearing_type:
            bearing.click()

    def search(self, number):
        self.driver.find_element_by_class_name("search-input").send_keys(number)

    def wait_for_table(self):
        sleep(3)
        wait = WebDriverWait(self.driver, 30)
        wait.until(EC.visibility_of_element_located((By.CLASS_NAME, "bearing-table ")))

    def select_row(self, designation_value):
        value = self.driver.find_element_by_class_name("skf-explorer")
        if value.text.strip() == designation_value.strip():
            value.click()
            self.scroll_down_page()

    def scroll_down_page(self):
        self.driver.execute_script("document.getElementsByClassName('previous-next-buttons')[0].scrollIntoView(false)")

    def next_button_colour_verify(self):
        colour = self.driver.find_element_by_class_name("button-default").value_of_css_property("background-color")
        actual_number = "rgba(72, 90, 100, 1)"
        testcase.assertEqual(colour, actual_number, "button colour is grey")










