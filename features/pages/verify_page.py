from time import sleep
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

url = "https://www.skfbearingselect.com"


class VerifyDropdown:

    def __init__(self, driver):
        self.driver = driver
        self.all_present = True

    def open_application(self):
        self.driver.get(url)
        sleep(5)
        wait = WebDriverWait(self.driver, 30)
        wait.until(EC.presence_of_element_located((By.CLASS_NAME, "button-default")))

    def click_on_accept_continue(self):
        if self.driver.find_element_by_class_name("button-default").is_displayed():
            self.driver.find_element_by_class_name("button-default").click()

    def click_on_image(self, image_name):
        name = self.driver.find_element_by_class_name("selection-label")
        if name.text == image_name:
            name.click()

    def select_bearing_type(self):
        self.driver.find_element_by_css_selector("[aria-label='Select bearing type']").click()
        wait = WebDriverWait(self.driver, 30)
        wait.until(EC.presence_of_element_located((By.CLASS_NAME, "cdk-overlay-pane")))

    def verify_options(self, user_input):
        all_type = {}
        bearing_type = self.driver.find_elements_by_css_selector(".mat-select-panel>mat-option>span.mat-option-text>span")
        for name in bearing_type:
            bearing_text = name.text
            all_type[bearing_text] = 1
        for row in user_input:
            if all_type[row[0]] != 1:
                self.all_present = False
                return

    def close_dropdown(self):
        self.driver.find_element_by_class_name("cdk-overlay-backdrop").click()