I am using Pychram IDE ,python,selenium,behave BDD Framework for writting test cases.

Need to Install:

1.Python 3.4.3 ---> First check if you have python version 3.x or not. Open command terminal, python --version ---->If you don't have, install from below link
https://www.python.org/downloads/

2. Set python environment variable in path.

3. Download Pycharm IDE (Community)and install from below link.
https://www.jetbrains.com/pycharm/download/#section=windows

4.Selenium 3.141.0 ---> pip install selenium (from terminal) ---->from selenium import webdriver by using ALT + Enter (from Pycharm IDE)

5.Install package behave 1.2.6 ---> pip install behave (from terminal) ----> from behave import * by using ALT + Enter (from Pycharm IDE)

6.Clone the repository (Automate_SKF from below URL) in your local machine.
https://bitbucket.org/swagatika21/automate_skf/src/master/

7.Open the repository in Pycharm and run the scenarios.



